
import 'dart:ui';

import 'package:correo_app/models/request_email.dart';
import 'package:flutter/material.dart';
import 'package:correo_app/auth.dart';
import 'package:correo_app/data/database_helper.dart';
import 'package:correo_app/models/correo.dart';
import 'package:correo_app/screens/login/login_screen_presenter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen>
    implements LoginScreenContract, AuthStateListener {
  BuildContext _ctx;

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _username, _password, _buzon, _cantidad;
  int numero;
  LoginScreenPresenter _presenter;

  LoginScreenState() {
    _presenter = new LoginScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  void _submit() {
   Future<Correo> post = consultarPost();
    showDialog(context: context, child:
    new AlertDialog(
      title: new Text("Mensaje"),
      content: FutureBuilder<Correo>(
          future: post,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Text(snapshot.data.desde);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
          }
      )
    )
    );

    /*final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doLogin(_username, _password, _buzon, numero);
    }*/
  }


  Future<Correo> consultarPost() async {

    RequestEmail rq = new RequestEmail();
    final response =
    await http.post('http://192.168.5.76:8001/lenguajes/correo/test',

        body: {
          "user": "",
          "password": "",
          "busqueda": "",
          "cantidad": ""
        }); // post api call
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Correo.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }


  Future<Correo> fetchPost() async {
    final response =
    await http.get('http://192.168.5.76:8001/lenguajes/correo/test1');

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Correo.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  onAuthStateChanged(AuthState state) {

    if(state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Revisar"),
      color: Colors.primaries[10],
    );
    var loginForm = new Column(
      children: <Widget>[
        new Text(
          //"Revision de correo",
          "Test",
          textScaleFactor: 2.0,
        ),
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField(
                  onSaved: (val) => _username = val,
                  validator: (val) {
                    return val.length < 5
                        ? "Nombre de Usuario debe tener mas de 5 letras"
                        : null;
                  },
                  decoration: new InputDecoration(labelText: "Correo"),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField(
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password"),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField(
                  onSaved: (val) => _buzon = val,
                  validator: (val) {
                    return val.length < 5
                        ? "Nombre de buzon debe tener mas de 5 letras"
                        : null;
                  },
                  decoration: new InputDecoration(labelText: "Buzon de busqueda"),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField(
                  onSaved: (val) => _cantidad = val,
                  validator: (val) {
                    var myint = int.parse(val);
                    numero = myint is int ? myint : null;
                    return myint is int
                        ? null
                        : "Debe ser un entero";
                  },
                  decoration: new InputDecoration(labelText: "Cantidad de correos a consultar"),
                ),
              ),
            ],
          ),
        ),
        _isLoading ? new CircularProgressIndicator() : loginBtn
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return new Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("images/login_background.jpg"),
              fit: BoxFit.cover),
        ),
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                child: loginForm,
                height: 500.0,
                width: 300.0,
                decoration: new BoxDecoration(
                    color: Colors.grey.shade200.withOpacity(0.5)),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onLoginSuccess(Correo user) async {
    _showSnackBar(user.toString());
    setState(() => _isLoading = false);
    var db = new DatabaseHelper();
    //await db.saveUser(user);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
  }
}
