import 'package:correo_app/data/rest_ds.dart';
import 'package:correo_app/models/correo.dart';

abstract class LoginScreenContract {
  void onLoginSuccess(Correo user);
  void onLoginError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  RestDatasource api = new RestDatasource();
  LoginScreenPresenter(this._view);

  doLogin(String username, String password, String busqueda, int cantidad) {
    api.login(username, password, busqueda, cantidad).then((Correo user) {
      _view.onLoginSuccess(user);
    }).catchError((Exception error) => _view.onLoginError(error.toString()));
  }
}