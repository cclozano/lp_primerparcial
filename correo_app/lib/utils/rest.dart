
import 'dart:async';
import 'dart:convert';

import 'package:correo_app/models/correo.dart';
import 'package:correo_app/models/request_email.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<Correo> fetchPost() async {
  final response =
  await http.get('http://192.168.1.121:8001/lenguajes/correo/test1');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Correo.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<Correo> consultarPost(String username, String password, String busqueda, String cantidad) async {

  RequestEmail rq = new RequestEmail();
  final response =
  await http.post('http://192.168.1.121:8001/lenguajes/correo/test',
      body: {
      "user": username,
      "password": password,
  }); // post api call
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Correo.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}


 void main() => runApp(MyApp(post: consultarPost('', '', '', '1')));
//void main() => runApp(MyApp(post: fetchPost()));

class MyApp extends StatelessWidget {
  final Future<Correo> post;

  String _username, _password, _buzon, _cantidad;

  MyApp({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Ejemplo de consumir REST'),
        ),
        body: Center(
          child: FutureBuilder<Correo>(
            future: post,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.desde);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}