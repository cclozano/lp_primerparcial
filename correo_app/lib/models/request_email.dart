class RequestEmail {
  String user;
  String password;
  String busqueda;
  String cantidad;

  RequestEmail({this.user, this.password, this.busqueda, this.cantidad});

  RequestEmail.fromJson(Map<String, dynamic> json) {
    user = json['user'];
    password = json['password'];
    busqueda = json['busqueda'];
    cantidad = json['cantidad'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user'] = this.user;
    data['password'] = this.password;
    data['busqueda'] = this.busqueda;
    data['cantidad'] = this.cantidad;
    return data;
  }
}