class Correo {
  String date;
  String to;
  String content;
  String desde;
  String subject;
  String estado;

  Correo(
      {this.date,
        this.to,
        this.content,
        this.desde,
        this.subject,
        this.estado});

  Correo.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    to = json['to'];
    content = json['content'];
    desde = json['desde'];
    subject = json['subject'];
    estado = json['estado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['to'] = this.to;
    data['content'] = this.content;
    data['desde'] = this.desde;
    data['subject'] = this.subject;
    data['estado'] = this.estado;
    return data;
  }
}
