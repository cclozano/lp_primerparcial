import 'dart:async';

import 'package:correo_app/utils/network_util.dart';
import 'package:correo_app/models/correo.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "http://192.168.5.76:8001/lenguajes/correo";
  static final LOGIN_URL = BASE_URL + "/test";

  Future<Correo> login(String username, String password, String busqueda, int cantidad) {
    return _netUtil.post(LOGIN_URL, body: {
      "user": username,
      "password": password,
      "busqueda": busqueda,
      "cantidad": cantidad
    }).then((dynamic res) {
      print(res.toString());
      if(res["error"]) throw new Exception(res["error_msg"]);
      return new Correo.fromJson(res['body']);
    });
  }
}