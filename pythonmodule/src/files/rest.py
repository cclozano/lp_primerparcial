import falcon
import mylexer
import php_lexer
import php_parser
import leerV2
import json

class LexResource:
    def on_post(self, req, resp):
        message = req.media.get('message')
        resultado = mylexer.obtenerTokens(message)
        resp.media = {'tokens': resultado}
        resp.status = falcon.HTTP_200


class PlagioResource:
    def on_post(self, req, resp):
        codigo1 = req.media.get('codigo1')
        codigo2 = req.media.get('codigo2')
        print(codigo2)
        print(codigo1)
        resultado = mylexer.plagioLex2(codigo1,codigo2)
        resp.media=resultado
        resp.status = falcon.HTTP_200

class SintaxisResource:
    def on_post(self, req, resp):
        codigo = req.media.get('codigo')
        print (codigo)
        resultado = php_parser.sintaxis(codigo)
        resp.media=resultado
        resp.status = falcon.HTTP_200

class EmailResource:
    def on_post(self, req, resp):
        user = req.media.get('user')
        password = req.media.get('password')
        busqueda = req.media.get('busqueda')
        cantidad = req.media.get('cantidad')

        resultado = leerV2.revisarMail(user,password,busqueda,cantidad)

        json_string = json.dumps([ob.__dict__ for ob in resultado])
        print json_string
        resp.media = json_string
        resp.status = falcon.HTTP_200


api = falcon.API()
api.add_route('/lex', LexResource())
api.add_route('/plagio',PlagioResource())
api.add_route('/sintaxis',SintaxisResource())

api.add_route('/email',EmailResource())