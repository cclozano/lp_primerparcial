#pip install beautifulsoup4
import email
import imaplib
from bs4 import BeautifulSoup
import json


class Correo:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)




def login(usuario,contrasena):
    mail = imaplib.IMAP4_SSL("imap.gmail.com")
    mail.login(usuario, contrasena)
    return mail

def listado(mail , caja):
    # select LAST
    mail.select(caja)
    # mail.list()
    result, data = mail.uid("search", None, "ALL")
    # print(result)
    # print(data)
    dataList = data[0].split()
    # print(dataList)
    #ultimo = dataList[-1]
    # print(ultimo)
    return dataList

def recorrido(cantidad, dataList, mail):
    # bucle fetch


    my_list = []

    for i in range(cantidad):
        result2, emailData = mail.uid("fetch", dataList[-1-i], "(RFC822)")
        # print("email data")
        # print(emailData) imprime esto [(b'17179 (UID 17425 RFC822 {13344}', b'MIME-Version: 1.0\r\nDate: Fri, 21 Dec 2018 09:19:15 -0500\r\nReferences: <cb72ae6bee9d3234d49b46c916bf231c@bancointernacional.com.ec>\r\nIn-Reply-To: <cb72ae6bee9d3234d49b46c916bf231c@bancointernacional.com.ec>\r\nMessage-ID: <CABaUGgqO6ycJ6cZWeEeM=0tFboNgqunCRc=M_XFSsBQiysmcwQ@mail.gmail.com>\r\nSubject: =?UTF-8?Q?Fwd=3A_Horarios_de_atenci=C3=B3n_feriado_de_Navidad?=\r\nFrom: Matheus Lopez <matheusjlv@gmail.com>\r\nTo: Matheus Pulido <matheusjlv@gmail.com>\r\nContent-Type: multipart/alternative; boundary="000000000000fe82dc057d88edc8"\r\nX-Antivirus: AVG (VPS 181221-4, 21/12/2018), Inbound message\r\nX-Antivirus-Status: Clean\r\n\r\n--000000000000fe82dc057d88edc8\r\nContent-Type: text/plain; charset="UTF-8"\r\nContent-Transfer-Encoding: quoted-printable\r\n\r\n---------- Forwarded message ---------\r\nFrom: Banco Internacional <baninternews@bancointernacional.com.ec>\r\nDate: jue., 20 dic. 2018 a las 13:42\r\nSubject: Horarios de atenci=C3=B3n feriado de Navidad\r\nTo: <matheusjlv@gmail.com>\r\n\r\n\r\n\r\n\r\nPara asegurar la entrega de nuestros e-mail en tu correo, por favor agrega\r\nbaninternews@bancointernacional.com.ec a tu libreta de direcciones de\r\ncorreo.\r\nSi no visualizas correctamente este mail, da click aqu=C3=AD\r\n<http://trk.masterbase.com/v3/MB/43BE8FF8FC213E8AFDF5C6C0803C9A652680623DB9=\r\n0BB64BA4F340F4E5B67B27D6B4F654DC26AFF3A14CD1462C91735C96FF41BA13263CE1C6FA3=\r\n4720E41E626DD58DEA1CA158B217C6E595ACC1885E204F98B62D0E41F0AD08EC18E6F3F04C3=\r\n366A39075C1CB684A001BD7D421E3A965DAEDDABC13F1A640969FE7097C80D73DAE569C57ED=\r\nCBFE2E3D70B085171935CBB8B21F572BC8F2AD431647C8D17FAE5A4F59D4D13E5D817A0A41D=\r\nC146F94A7105C9C11BA2FEEC59EDEBC1C89600F8E04296AE8B6FB55B781E12FD55FCFFF1DDD=\r\n1A3A6DE0C087B3CC0A7547E6A0FB29BBD8885B1AEC78AEBE48C06C9450D9C2A65BD9B14937D=\r\nC0DC34461310C66AEC81589FE2B66C829B2152C8EA696988561F56D17CFCD157E4D268B6138=\r\n8384FFE5A93E1EC0EE8FB2A6E2AFAF7C08EC9F0AF97746F45692327D018552D25953FE0F574=\r\n7CB492CEC0FF8ED8549851DDD2DF61155DB2849E3238BE>\r\n*ADVERTENCIA IMPORTANTE*\r\n\r\nNo solicitamos claves. No pedimos actualizaci=C3=B3n de datos. No enviamos =\r\nlinks\r\npor correo electr=C3=B3nico.\r\n*Si recibes un link o un correo sospechoso, comun=C3=ADcate al:*\r\n*1700 360 360* o desde el Austro al (02) 394 0200 para verificar su\r\nautenticidad.\r\n\r\nInformaci=C3=B3n del Correo: Favor no respondas a este mensaje con la opci=\r\n=C3=B3n\r\n"responder" (reply). Para mayor informaci=C3=B3n comun=C3=ADcate con nuestr=\r\na Atenci=C3=B3n\r\nTelef=C3=B3nica al 1700 360 360 o por correo electr=C3=B3nico para tus suge=\r\nrencias y\r\ncomentarios a bancaonline@bancointernacional.com.ec. Este correo\r\nelectr=C3=B3nico est=C3=A1 dirigido exclusivamente al receptor original. (*=\r\n) Si se\r\nencuentra en el Austro digita (02) 394 0200\r\n\r\nNota de descargo: La informaci=C3=B3n contenida en este mensaje es de propi=\r\nedad\r\nde Banco Internacional S.A., y no debe considerarse una oferta, promesa,\r\ncontrato o asesor=C3=ADa, a menos que as=C3=AD lo establezca expresamente e=\r\nl Banco y\r\nse suscriban los documentos legales correspondientes.\r\n\r\nDe acuerdo a la Ley de Comercio Electr=C3=B3nico del Ecuador y su Reglament=\r\no\r\npublicado en el Registro Oficial 735 del 31 de diciembre de 2002, Decreto\r\nN=C2=BA 3496, Art=C3=ADculo 22.- Env=C3=ADo de mensajes de datos no solicit=\r\nados, tu puedes\r\npedir el cese del env=C3=ADo de informaci=C3=B3n en cualquier momento. Todo=\r\n mensaje\r\nelectr=C3=B3nico que cuente con la opci=C3=B3n de desuscripci=C3=B3n no se =\r\nconsidera SPAM.\r\nSi no deseas recibir nuestros correos o requieres darte de baja da clic\r\naqu=C3=AD en de-suscribirse "unsubscribe"\r\n<http://trk.masterbase.com/v3/MB/43BE8FF8FC213E8AFDF5C6C0803C9A652680623DB9=\r\n0BB64BA4F340F4E5B67B27D6B4F654DC26AFF3F4F52D68CBF4942296FF41BA13263CE1AAFD5=\r\nEEA9B64548A4C1FC7C6E7DBE443EE579D273F99A1B9AF466C48599FCBBAEDD537D45D3E982E=\r\nF73E4A91BEF5670AC1962AEF009EC9D43B2F951D26CA3D9980E314E9B759E599250B4FC031E=\r\n28341AAE2B29E4D9D8652BAD8DE73A842B36BD1A3A6DE0C087B3CA932A575F86BBF4FB5C83C=\r\n1E451FCB2711FA0B0BEBBCE1F6C0707D301F5DDE1DE65647972268F8AA2A356F15DED62555D=\r\nFCA82AAD6F56D58E58AFDC9E95F4954EE7F49EC8D8CC55DDE02217FEC29D92ACB5551D46AA7=\r\n740B3ED86F67BB0AF0E85F33AA223A0D82CA3BC969D01277F0311F715C2D6F1160223955333=\r\nF024033B1FBC12AFAFDB451CDB4028C575DB1E8D2704F69474D8F59D1>\r\n.\r\n\r\n--000000000000fe82dc057d88edc8\r\nContent-Type: text/html; charset="UTF-8"\r\nContent-Transfer-Encoding: quoted-printable\r\n\r\n<div dir=3D"ltr"><br><br><div class=3D"gmail_quote"><div dir=3D"ltr">------=\r\n---- Forwarded message ---------<br>From: <strong class=3D"gmail_sendername=\r\n" dir=3D"auto">Banco Internacional</strong> <span dir=3D"ltr">&lt;<a href=\r\n=3D"mailto:baninternews@bancointernacional.com.ec">baninternews@bancointern=\r\nacional.com.ec</a>&gt;</span><br>Date: jue., 20 dic. 2018 a las 13:42<br>Su=\r\nbject: Horarios de atenci=C3=B3n feriado de Navidad<br>To:  &lt;<a href=3D"=\r\nmailto:matheusjlv@gmail.com">matheusjlv@gmail.com</a>&gt;<br></div><br><br>\r\n\r\n=09\r\n=09\r\n\r\n<div bgcolor=3D"#f2f2f2">\r\n<div align=3D"center">\r\n<table align=3D"center" border=3D"0" cellpadding=3D"0" cellspacing=3D"0" id=\r\n=3D"m_-9104729095128165993table27" width=3D"761">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td height=3D"91" style=3D"color:rgb(102,102,102);line-height:12px;font-=\r\nfamily:verdana;font-size:10px;text-decoration:none">\r\n\t\t\t<div align=3D"center">\r\n\t\t\t<p><br>\r\n\t\t\t<br>\r\n\t\t\tPara asegurar la entrega de nuestros e-mail en tu correo, por favor agre=\r\nga<br>\r\n\t\t\t<span style=3D"color:rgb(242,143,32);font-family:verdana;font-size:10px;=\r\ntext-decoration:none"><a href=3D"mailto:baninternews@bancointernacional.com=\r\n.ec" target=3D"_blank">baninternews@bancointernacional.com.ec</a> </span>a =\r\ntu libreta de direcciones de correo.<br>\r\n\t\t\tSi no visualizas correctamente este mail, da <a href=3D"http://trk.maste=\r\nrbase.com/v3/MB/43BE8FF8FC213E8AFDF5C6C0803C9A652680623DB90BB64BA4F340F4E5B=\r\n67B27D6B4F654DC26AFF3A14CD1462C91735C96FF41BA13263CE1C6FA34720E41E626DD58DE=\r\nA1CA158B217C6E595ACC1885E204F98B62D0E41F0AD08EC18E6F3F04C3366A39075C1CB684A=\r\n001BD7D421E3A965DAEDDABC13F1A640969FE7097C80D73DAE569C57EDCBFE2E3D70B085171=\r\n935CBB8B21F572BC8F2AD431647C8D17FAE5A4F59D4D13E5D817A0A41DC146F94A7105C9C11=\r\nBA2FEEC59EDEBC1C89600F8E04296AE8B6FB55B781E12FD55FCFFF1DDD1A3A6DE0C087B3CC0=\r\nA7547E6A0FB29BBD8885B1AEC78AEBE48C06C9450D9C2A65BD9B14937DC0DC34461310C66AE=\r\nC81589FE2B66C829B2152C8EA696988561F56D17CFCD157E4D268B61388384FFE5A93E1EC0E=\r\nE8FB2A6E2AFAF7C08EC9F0AF97746F45692327D018552D25953FE0F5747CB492CEC0FF8ED85=\r\n49851DDD2DF61155DB2849E3238BE" style=3D"color:rgb(242,143,32);text-decorati=\r\non:none" target=3D"_blank">click aqu=C3=AD</a></p>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<table align=3D"center" bgcolor=3D"#fafafa" border=3D"0" cellpadding=3D"0" =\r\ncellspacing=3D"0" style=3D"color:rgb(109,110,113);font-family:Arial,Helveti=\r\nca,sans-serif;font-size:16px;border-collapse:collapse" width=3D"761">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<table align=3D"center" border=3D"0" cellpadding=3D"0" cellspacing=3D"0"=\r\n width=3D"761">\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td width=3D"761">\r\n\t\t\t\t\t\t<table align=3D"center" border=3D"0" cellpadding=3D"0" cellspacing=3D=\r\n"0" width=3D"762">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td colspan=3D"1" width=3D"762">\r\n\t\t\t\t\t\t\t\t\t<table border=3D"0" cellspacing=3D"0" width=3D"762">\r\n\t\t\t\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\r\n\t\t\t\t\t\t\t\t\t<table border=3D"0" width=3D"758">\r\n\t\t\t\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\r\n\t\t\t\t\t\t\t\t\t<table border=3D"0" cellpadding=3D"0" cellspacing=3D"0" width=3D"7=\r\n62">\r\n\t\t\t\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td align=3D"center" width=3D"760"><img alt=3D"" height=3D"1138=\r\n" src=3D"https://ci4.googleusercontent.com/proxy/uYdKiSO0g3Cpw0uI9W_J2tW3_h=\r\nRpE-eE6mCsmXbNcCb7e11zLv343F_rVxsbglki8DCUl6Nd7JzMnJI5_fW7PiBI4RivLN9S3LHkE=\r\ntZf-nSRvvR6A6cvf9cVT4bSt2dUFcjzR_ZlsmX8uRzrmaCp=3Ds0-d-e1-ft#http://images.=\r\nmasterbase.com/v1/bancointermktec/b/1/BI_HORARIO-FERIADO-DICIEMBRE-navidad.=\r\njpg" usemap=3D"#m_-9104729095128165993_Map" width=3D"761"></td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td align=3D"center" colspan=3D"4" style=3D"padding:25px;color:rgb=\r\n(109,110,113);font-size:14px">\r\n\t\t\t\t\t\t\t\t\t<div align=3D"center" style=3D"color:rgb(247,147,29)"><strong>ADVE=\r\nRTENCIA IMPORTANTE</strong></div>\r\n\t\t\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t\t\tNo solicitamos claves. No pedimos actualizaci=C3=B3n de datos. No =\r\nenviamos links por correo electr=C3=B3nico.<br>\r\n\t\t\t\t\t\t\t\t\t<strong>Si recibes un link o un correo sospechoso, comun=C3=ADcate=\r\n al:</strong><br>\r\n\t\t\t\t\t\t\t\t\t<span style=3D"font-style:italic"><span style=3D"color:rgb(247,147=\r\n,29)"><strong>1700 360 360</strong></span> o desde el Austro al (02) 394 02=\r\n00 para verificar su autenticidad.</span></td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div align=3D"center">\r\n<table bgcolor=3D"#f2f2f2" border=3D"0" cellpadding=3D"0" style=3D"border-c=\r\nollapse:collapse" width=3D"761">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td bgcolor=3D"#ededed" style=3D"padding:25px">\r\n\t\t\t<p align=3D"justify"><font face=3D"Arial"><span style=3D"font-size:7pt">=\r\n<font color=3D"#f28f20">Informaci=C3=B3n del Correo: </font> <font color=3D=\r\n"#333333">Favor no respondas a este mensaje con la opci=C3=B3n &quot;respon=\r\nder&quot; (reply). Para mayor informaci=C3=B3n comun=C3=ADcate con nuestra =\r\nAtenci=C3=B3n Telef=C3=B3nica al 1700 360 360 o por correo electr=C3=B3nico=\r\n para tus sugerencias y comentarios a <a href=3D"mailto:bancaonline@bancoin=\r\nternacional.com.ec" style=3D"text-decoration:none" target=3D"_blank"> <font=\r\n color=3D"#f28f20">bancaonline@bancointernacional.com.ec</font></a>. Este c=\r\norreo electr=C3=B3nico est=C3=A1 dirigido exclusivamente al receptor origin=\r\nal. (*) Si se encuentra en el Austro digita (02) 394 0200 </font><br>\r\n\t\t\t<br>\r\n\t\t\t<font color=3D"#f28f20">Nota de descargo:</font><font color=3D"#333333">=\r\n La informaci=C3=B3n contenida en este mensaje es de propiedad de Banco Int=\r\nernacional S.A., y no debe considerarse una oferta, promesa, contrato o ase=\r\nsor=C3=ADa, a menos que as=C3=AD lo establezca expresamente el Banco y se s=\r\nuscriban los documentos legales correspondientes.<br>\r\n\t\t\t<br>\r\n\t\t\tDe acuerdo a la Ley de Comercio Electr=C3=B3nico del Ecuador y su Reglam=\r\nento publicado en el Registro Oficial 735 del 31 de diciembre de 2002, Decr=\r\neto N=C2=BA 3496, Art=C3=ADculo 22.- Env=C3=ADo de mensajes de datos no sol=\r\nicitados, tu puedes pedir el cese del env=C3=ADo de informaci=C3=B3n en cua=\r\nlquier momento. Todo mensaje electr=C3=B3nico que cuente con la opci=C3=B3n=\r\n de desuscripci=C3=B3n no se considera SPAM. Si no deseas recibir nuestros =\r\ncorreos o requieres darte de baja</font> <font color=3D"#0093d3"> <a href=\r\n=3D"http://trk.masterbase.com/v3/MB/43BE8FF8FC213E8AFDF5C6C0803C9A652680623=\r\nDB90BB64BA4F340F4E5B67B27D6B4F654DC26AFF3F4F52D68CBF4942296FF41BA13263CE1AA=\r\nFD5EEA9B64548A4C1FC7C6E7DBE443EE579D273F99A1B9AF466C48599FCBBAEDD537D45D3E9=\r\n82EF73E4A91BEF5670AC1962AEF009EC9D43B2F951D26CA3D9980E314E9B759E599250B4FC0=\r\n31E28341AAE2B29E4D9D8652BAD8DE73A842B36BD1A3A6DE0C087B3CA932A575F86BBF4FB5C=\r\n83C1E451FCB2711FA0B0BEBBCE1F6C0707D301F5DDE1DE65647972268F8AA2A356F15DED625=\r\n55DFCA82AAD6F56D58E58AFDC9E95F4954EE7F49EC8D8CC55DDE02217FEC29D92ACB5551D46=\r\nAA7740B3ED86F67BB0AF0E85F33AA223A0D82CA3BC969D01277F0311F715C2D6F1160223955=\r\n333F024033B1FBC12AFAFDB451CDB4028C575DB1E8D2704F69474D8F59D1" target=3D"_bl=\r\nank"><font color=3D"#f28f20">da clic aqu=C3=AD en de-suscribirse &quot;unsu=\r\nbscribe&quot; </font></a></font>.</span></font></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>=C2=A0</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n<map name=3D"m_-9104729095128165993_Map"><area coords=3D"540,466,651,501" h=\r\nref=3D"http://trk.masterbase.com/v3/MB/43BE8FF8FC213E8AFDF5C6C0803C9A652680=\r\n623DB90BB64BA4F340F4E5B67B27D6B4F654DC26AFF3FCA91D4FBAC6F75596FF41BA13263CE=\r\n1AAFD5EEA9B64548A4C1FC7C6E7DBE443EE579D273F99A1B9AF466C48599FCBBA40D8C99027=\r\n90D35EE56890B63F1239CD4133D93099CE18C972355B4605ECF64436BA408F20E15BBDF35F3=\r\n85FC3619DC8511FB16D14DE46551BC1505EE382D9D66940B07623200800A8492676E01FB767=\r\n4448A083906328B8A291A8326F6A19CC60F4D9107ECE62A77E22C92C376C1BC1CFED137D9A4=\r\n2BD2017638D39461DC927734E44BD4556BA420DB04D16ED2307CF990DD82F3A5752C7E973DE=\r\n79D2FFCC68EF20512090ED94A52F64CCE526B4B51A936AAF730EF6880CCC5DA4DE25CC0F65D=\r\n83D1261DABBB0B513A397AF4895DC7DD492428522980F82726B7DAA24DE15EA491F48FEAECA=\r\n0ADC8C7DC1D9BBAD44CA" shape=3D"rect" target=3D"_blank"></map></div>\r\n<img src=3D"https://ci5.googleusercontent.com/proxy/po0q3PlmmEBETsYG9U3a4o4=\r\nzrO4B8oTmniIHoHl-QDHiUfTMuDzvE1LAvwFzHeRrtT08CIm789iL1nYsT_j7S10b1fH-S5zPjw=\r\n7XR0MpjKLBvk00DMgLOOFkC6_TmSv7ztHBkMxIyReukA7VGEPmBjSQKWQuYXSKye6y1j7ECJ7qo=\r\nhHZJpX9VomB7UtWmlF50zdS9SRc6bG7_7zvBNKgO1R_D5UmtPhWr6b3y_vXXPWlQhNzmsgQbWLI=\r\nZEViWv6YzuDT3UyiNcw6ImyzOyDz3pAlF8xdcHTB2_DU4-fO=3Ds0-d-e1-ft#http://trk.ma=\r\nsterbase.com/v3/MB/AB008D63ED707B73AF5438F8F447E6D13F96635C2B9B2666CAB84302=\r\n0EB2246C6DA339AF22FC17572301CF16BB04859F07A0829D07174B94EECEFE717BB58E78C3F=\r\nB978C5B1369545B752E1830CD38EC/blank.png" border=3D"0" height=3D"1" width=3D=\r\n"1"></div>\r\n\r\n</div></div>\r\n\r\n--000000000000fe82dc057d88edc8--'), b')']

        rawEmail = emailData[0][1].decode("utf-8")
        emailMessage = email.message_from_string(rawEmail)
        # print("rawEmail")
        # print(rawEmail)
        # print("emailMessage")
        # print(emailMessage) #imprime con tags y con utf-8 y mas coding
        # print("directorio")
        # print(dir(emailMessage)) imprime ['__bytes__', '__class__', '__contains__', '__delattr__', '__delitem__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_charset', '_default_type', '_get_params_preserve', '_headers', '_payload', '_unixfrom', 'add_header', 'as_bytes', 'as_string', 'attach', 'defects', 'del_param', 'epilogue', 'get', 'get_all', 'get_boundary', 'get_charset', 'get_charsets', 'get_content_charset', 'get_content_disposition', 'get_content_maintype', 'get_content_subtype', 'get_content_type', 'get_default_type', 'get_filename', 'get_param', 'get_params', 'get_payload', 'get_unixfrom', 'is_multipart', 'items', 'keys', 'policy', 'preamble', 'raw_items', 'replace_header', 'set_boundary', 'set_charset', 'set_default_type', 'set_param', 'set_payload', 'set_raw', 'set_type', 'set_unixfrom', 'values', 'walk']

        to_ = emailMessage["To"]
        from_ = emailMessage["From"]
        subject_ = emailMessage["Subject"]
        date_ = emailMessage["Date"]
        # print(emailMessage.get_payload())
        # SegundoBucle
        counter = 1
        for part in emailMessage.walk():
            if (part.get_content_maintype() == "multipart"):
                continue
            filename = part.get_filename()
            if not filename:
                ext = "html"
                # filename = "msg-part-%08d%" %(counter, ext)
            counter += 1

        content_type = part.get_content_type()
        print(content_type)
        if ("plain" in content_type):
            # para leer texto llano
            print("payload")
            print(part.get_payload())
        elif "html" in content_type:
            html_ = part.get_payload()
            soup = BeautifulSoup(html_, "html.parser")
            text = soup.get_text()
            print("beautifull soup")
            print(text)
        else:
            print(content_type)

        print(to_)
        print(from_)
        print(subject_)
        print(date_)

        me = Correo()
        me.to = to_
        me.desde = from_
        me.subject = subject_
        me.date = date_

        my_list.append(me)

    return my_list

    # emailMessage.get_payload()



def revisarMail(user, password, busqueda, cantidad):
    correo = login(user,password)
    lista=listado(correo,busqueda)
    return recorrido(cantidad,lista,correo)



#usuario=str(input("Ingrese su correo: "))
#contrasena=str(input("Ingrese su contrasena: "))


#mail = login(usuario,contrasena)

#caja=str(input("Ingresar caja de busqueda: "))

#dataList=listado(mail,caja)

#cantidad=int(input("cuantos mails desea revisar? "))


#recorrido(cantidad,dataList,mail)