# coding=utf-8
import ply.yacc as yacc
import mylexer

tokens = mylexer.tokens
tokens = mylexer.tokens
precedence = (
   ('left', 'ELSEIF'),
   ('left', 'ELSE')
)
#DEFINICION DE UNA OPCION VACIA
def p_empty(p):
   'empty :'
   pass

def p_asignar(p):
 '''asignar : NAMEVARS IGUAL expresion'''

def p_expresion(t):
  '''expresion : expresion SUMA termino
                | expresion RESTA termino
                | termino'''

def p_termino(t):
  '''termino : termino MULTIPLICACION factor
                | termino DIVISION factor
                | factor'''

def p_factor(p):
 '''factor : NUMERO'''

#opeaciones logicas
def p_operacionlogica(t):
  '''operacionlogica : operacionlogicanum
                | operacionlogicastr
             	| NAMEVARS operadorlogiconum NAMEVARS'''

def p_operacionlogicanum(t):
  '''operacionlogicanum : expresion operadorlogiconum expresion
                | expresion operadorlogiconum NAMEVARS
                | NAMEVARS operadorlogiconum expresion'''

def p_operacionlogicastr(t):
  '''operacionlogicastr : TEXTO operadorlogicostr TEXTO
                | NAMEVARS operadorlogicostr TEXTO
             	| TEXTO operadorlogicostr NAMEVARS'''

def p_operadorlogicostr(t):
  '''operadorlogicostr : IGUALQUE
             	| DISTINTOQUE
             	'''

def p_operadorlogiconum(t):
  '''operadorlogiconum : MAYOR
                | MENOR
             	| MAYORIGUAL
             	| MENORIGUAL
                | operadorlogicostr
             	'''

#opeaciones logicas

#SOLO ESTA PARA TEXTO METIDO Y NUMEROS METIDOS, NO VARIABLES AUN
def p_substr(t):
  '''substring : SUBSTR "(" TEXTO "," NUMERO ")"
                | SUBSTR "(" TEXTO "," NUMERO "," NUMERO ")"
             	'''

def p_lista(t):
  '''lista : LIST "(" elementoslist ")"
             	'''

def p_elementoslist(t):
  '''elementoslist : elementoslist "," NAMEVARS
                | NAMEVARS
             	'''    

def p_arreglo(t):
  '''arreglo : ARRAY "(" elementosarray ")"
             	'''  

def p_elementosarray(t):
  '''elementosarray : elementosarray "," keys "=" ">" NAMEVARS
                | keys "=" ">" NAMEVARS
             	'''               	
def p_keys(t):
  '''keys : NAMEVARS
                | NUMERO
             	'''

#Listas#




#While
def p_iteracionwhile(t):
 '''iteracionwhile : WHILE IZQPARENTESIS condicion DERPARENTESIS COLON
    '''             
#ANDAND ES && Y OROR ES ||
def p_condicion(t):
 '''condicion :   operacionlogica AND condicion
                 | operacionlogica OR condicion
                 | operacionlogica OROR condicion
                 | operacionlogica ANDAND condicion
                 | TRUE AND condicion
                 | TRUE OR condicion
                 | TRUE OROR condicion
                 | TRUE ANDAND condicion
                 | FALSE AND condicion
                 | FALSE OR condicion
                 | FALSE OROR condicion
                 | FALSE ANDAND condicion
                 | operacionlogica
                 | TRUE
                 | FALSE
                 '''

#If
def p_iteracion_if(t):
 '''iteracion_if : IF IZQPARENTESIS condicion DERPARENTESIS COLON asignar
            | IF IZQPARENTESIS condicion DERPARENTESIS COLON asignar elseifelse
| IF IZQPARENTESIS condicion  DERPARENTESIS IZQCORCHETE asignar DERCORCHETE  
| IF IZQPARENTESIS condicion DERPARENTESIS IZQCORCHETE asignar DERCORCHETE elseifelse
            '''
def p_elseifelse(t):
 '''elseifelse : ELSE IZQCORCHETE asignar DERCORCHETE
     | ELSEIF IZQPARENTESIS condicion DERPARENTESIS IZQCORCHETE asignar DERCORCHETE elseifelse
     | empty '''




#FOR
def p_buclefor(t):
 '''buclefor : FOR IZQPARENTESIS declaracionvariable PUNTOYCOMA expresionfor PUNTOYCOMA expresionagregado DERPARENTESIS sentencia
             	  | FOR IZQPARENTESIS  PUNTOYCOMA  PUNTOYCOMA  DERPARENTESIS sentencia
    ''' 


def p_declaracionvariable(t):
    '''declaracionvariable : VARIABLE PUNTOYCOMA
                      | VARIABLE IGUAL NUMERO PUNTOYCOMA
                      | VARIABLE IGUAL boolean PUNTOYCOMA
                      | VARIABLE IGUAL VARIABLE PUNTOYCOMA
    '''
    

def p_expresionfor(t):
    '''expresionfor : var IGUAL expresionfor
             	 | expresionagregado operadorlogiconum expresionagregado
         | expresionagregado
             	 | var IGUAL AMPERSANT VARIABLE
         	     | expresionfor ANDAND expresionfor
             	 | expresionfor OROR expresionfor
    '''
    

def p_expresionagregado(t):
    '''expresionagregado : expresionagregado operacionagregar termino
                    	  | termino
                    	  | termino MENOSMENOS
                	      | termino MASMAS
    '''
    

def p_operacionagregar(t):
    '''operacionagregar : SUMA
         	| RESTA
    '''
    

def p_terminofor(t):
    '''termino : IZQPARENTESIS expresionfor DERPARENTESIS
         	 | var
         	 | NUMERO
         	 | boolean
    '''
    

def p_sentencia(t):
    '''sentencia : expresionfor PUNTOYCOMA
         	    | return
    '''
    

def p_return(t):
    '''return : RETURN PUNTOYCOMA
             	  | RETURN expresionfor PUNTOYCOMA
    '''

def p_boolean(p):
    '''boolean : TRUE
         	  | FALSE
    '''

def p_var(p):
    '''var : VARIABLE
     	  | VARIABLE IZQCORCHETE expresionfor DERCORCHETE
    '''



def p_fun_declaration(p):
    '''fun_declaration : FUNCTION ID LPAREN params RPAREN
                       | FUNCTION ID LPAREN params RPAREN compount_stmt
    '''
    pass





def p_error(p):
 raise Exception("Syntax error.")

#print("hola1")

yacc.yacc()

#print("hola2")


def sintaxis(code):
    parser = yacc.yacc()
    try:
        result = parser.parse(code)
        return result
    except:
        return  'ERROR'




