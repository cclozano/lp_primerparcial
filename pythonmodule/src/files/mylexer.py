# coding=utf-8

import re
import ply.lex as lex

#Palabras reservadas http://php.net/manual/es/reserved.keywords.php
reservadas = {
   '__halt_compiler()' : '__HALT_COMPILER',
   'abstract' : 'ABSTRACT',
   'and' : 'AND',
   'array()' : 'ARRAY',
   'as' : 'AS',
   'break' : 'BREAK',
   'callable' : 'CALLABLE',
   'case' : 'CASE',
   'catch' : 'CATCH',
   'class' : 'CLASS',
   'clone' : 'CLONE',
   'const' : 'CONST',
   'continue' : 'CONTINUE',
   'declare' : 'DECLARE',
   'default' : 'DEFAULT',
   'die()' : 'DIE',
   'do' : 'DO',
   'echo' : 'ECHO',
   'else' : 'ELSE',
   'elseif' : 'ELSEIF',
   'empty()' : 'EMPTY',
   'enddeclare' : 'ENDDECLARE',
   'endfor' : 'ENDFOR',
   'endforeach' : 'ENDFOREACH',
   'endif' : 'ENDIF',
   'endswitch' : 'ENDSWITCH',
   'endwhile' : 'ENDWHILE',
   'eval()' : 'EVAL',
   'exit()' : 'EXIT',
   'extends' : 'EXTENDS',
   'final' : 'FINAL',
   'finally' : 'FINALLY',
   'for' : 'FOR',
   'foreach' : 'FOREACH',
   'function' : 'FUNCTION',
   'global' : 'GLOBAL',
   'goto' : 'GOTO',
   'if' : 'IF',
   'implements' : 'IMPLEMENTS',
   'include' : 'INCLUDE',
   'include_once' : 'INCLUDE_ONCE',
   'instanceof' : 'INSTANCEOF',
   'insteadof' : 'INSTEADOF',
   'interface' : 'INTERFACE',
   'isset()' : 'ISSET',
   'list()' : 'LIST',
   'namespace' : 'NAMESPACE',
   'new' : 'NEW',
   'or' : 'OR',
   'print' : 'PRINT',
   'private' : 'PRIVATE',
   'protected' : 'PROTECTED',
   'public' : 'PUBLIC',
   'require' : 'REQUIRE',
   'require_once' : 'REQUIRE_ONCE',
   'return' : 'RETURN',
   'static' : 'STATIC',
   'switch' : 'SWITCH',
   'throw' : 'THROW',
   'trait' : 'TRAIT',
   'try' : 'TRY',
   'unset()' : 'UNSET',
   'use' : 'USE',
   'var' : 'VAR',
   'while' : 'WHILE',
   'xor' : 'XOR',
   'yield' : 'YIELD'
}


tokens = ['NOMBREFUN','IZQLLAVE','DERLLAVE','OPENTAG','CLOSETAG','NAMEVARS' , 'NAMEVARC' , 'NAMEVART' , 'SUMA', 'RESTA', 'MULTIPLICACION', 'DIVISION',
'RESIDUO', 'IGUAL',  'PUNTO', 'MASMAS', 'MENOSMENOS',  'MENORQUE', 'MAYORQUE', 'MENORIGUAL', 'MAYORIGUAL','MENOR','MAYOR',
'DIFERENTEDE',  'IGUALQUE', 'DISTINTOQUE','ANDAND','OROR','NEGACION',  'SUMAIGUAL', 'RESTAIGUAL', 'MULTIPLICAIGUAL',
'DIVISIONIGUAL', 'IGUALYTIPO', 'DIFERENTEYTIPO','NUMERO', 'SALTOLINEA', 'TEXTO', 'SUBSTR',
'IZQPARENTESIS', 'DERPARENTESIS', 'IZQCORCHETE', 'DERCORCHETE' ,'COLON',

#boolean
    'TRUE','FALSE',
'PUNTOYCOMA','AMPERSANT','VARIABLE'


] + list(reservadas.values())

def t_OPENTAG(t):
    r'(<\?(php)?)'
    return t

def t_CLOSETAG(t):
    r'\?>'
    return t

t_NOMBREFUN=r'([^"]*")|([^\']*\')'

t_IZQLLAVE= r'\{'
t_DERLLAVE= r'\}'

t_SUBSTR=r'substr'

#Sin considerar caracteres especiales a más de “_”
t_NAMEVARS=r'\$[a-zA-Z_][a-zA-Z0-9_]*'

#Para el complemento de las variables
t_NAMEVARC=r'(\[[1-9]([0-9])*\])+'

#Para asignar el $this
t_NAMEVART=r'\$\$[a-zA-Z_][a-zA-Z0-9_]*'

t_TEXTO=r'(("[^"]*")|(\'[^\']*\'))'



#la variable compuesta como tal sería
#             name : NAMEVARS │ NAMEVARS complement
#         complement : NAMEVARC │ NAMEVARC complement

t_ignore= " \t"
t_SUMA = r'\+'
t_RESTA = r'-'
t_MULTIPLICACION = r'\*'
t_DIVISION = r'/'
t_RESIDUO = r'\%'
t_IGUAL = r'='
t_PUNTO = r'\.'
t_NEGACION = r'!'

t_IZQPARENTESIS = r'\('
t_DERPARENTESIS = r'\)'
t_COLON     = r':'
t_IZQCORCHETE= r'\['
t_DERCORCHETE= r'\]'
def t_TRUE(t):
    r'true'
    return t

def t_FALSE(t):
    r'false'
    return t

t_PUNTOYCOMA = r';'
t_AMPERSANT = r'\&'
def t_VARIABLE(t):
   r'\$\w+(\d\w)*'
   return t


def t_ID(t):
   r'[a-zA-Z_][a-zA-Z_0-9]*'
   t.type = reservadas.get(t.value,'ID')    # palabras reservadas
   return t


def t_SUMAIGUAL(t):
    r'\+='
    return t
def t_RESTAIGUAL(t):
    r'-='
    return t
def t_MULTIPLICACIONIGUAL(t):
    r'\*='
    return t
def t_DIVISIONIGUAL(t):
    r'/='
    return t
def t_ANDAND(t):
    r'/&/&'
    return t
def t_OROR(t):
    r'\|\|'
    return t
def t_MASMAS(t):
    r'\+\+'
    return t
def t_MENOSMENOS(t):
    r'--'
    return t


def t_MENORQUE(t):
    r'--'
    return t
def t_MENORIGUAL(t):
    r'<='
    return t
def t_MENOR(t):
    r'<'
    return t
def t_MAYORIGUAL(t):
    r'>='
    return t
def t_MAYOR(t):
    r'>'
    return t
def t_DIFERENTEDE(t):
    r'<>'    
    return t
def t_IGUALQUE(t):
    r'=='
    return t
def t_DISTINTOQUE(t):
    r'!='
    return t
def t_IGUALYTIPO(t):
    r'==='
    return t
#HAY 2 != SON LO MISMO?
def t_DIFERENTEYTIPO(t):
    r'!=='
    return t



def t_error(t):
 print("Ilegal character")
 t.lexer.skip(1)

def t_NUMERO(token):
 r"-?[1-9][0-9]*"
 token.value = int(token.value)
 return token
#AUN NO ESTOY SEGURO DE COMO PROBAR EL SALTO
def t_SALTOLINEA(token):
 r"\n+"
 token.lexer.lineno += len(token.value)

def obtenerTokens(str):
    lex.lex()
    lex.input(str)
    resultado = []
    while (True):
        try:
            toc=lex.token()
            if not toc: break
            resultado.append([toc.type,toc.value])
        except:
            resultado.append(['****error****','No se reconoce'])
    return resultado



def tokensList(texto):
    lista=[]
    lex.lex()
    lex.input(texto)
    while True:
        try:
            toc=lex.token()
            if not toc: break
            lista.append(toc.type)
        except:
            continue
    print(lista)
    return lista

def plagioLex(codigo1, codigo2):
    lista1=tokensList(codigo1)
    lista2=tokensList(codigo2)
    total= max(len(lista1),len(lista2))
    numero=min(len(lista1),len(lista2))
    repetidos=0
    for i in range(numero):
        if(lista1[i]==lista2[i]):
            repetidos=repetidos+1
            print(lista1[i])
            print (lista2[i])
            print(repetidos)

    resultado=repetidos/total
    #print(resultado)
    return resultado*100


def plagioLex2(codigo1, codigo2):
    lista1=tokensList(codigo1)
    lista2=tokensList(codigo2)
    total= max(len(lista1),len(lista2))
    numero=min(len(lista1),len(lista2))
    repetidos=0.0
    for i in range(numero):
        if(lista1[i]==lista2[i]):
            repetidos=repetidos+1

    resultado=repetidos/total
    print repetidos
    print(resultado)
    print(total)
    return resultado*100



cod1 = 'for()'
cod2 = 'for() while'

print plagioLex2(cod1,cod2)

