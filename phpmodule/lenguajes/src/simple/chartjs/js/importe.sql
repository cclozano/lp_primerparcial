CREATE DATABASE PruebaBase;
USE pruebaBase;

CREATE table resultados
(
codigo int primary key NOT NULL,
tipo varchar(100) unique NOT NULL,
cantidad int not null
)engine InnoDB;

LOAD DATA INFILE 'prueba.txt'
into table resultados
fields terminated by ','
lines terminated by '\n';

select * from resultados
