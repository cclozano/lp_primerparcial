$(document).ready(function(){
	$.ajax({
		url: "http://localhost/chartjs/data.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var codigo = [];
			var tipo= [];
			var cantidad = [];

			for(var i in data) {
				codigo.push("Tipo " + data[i].codigo);
				cantidad.push(data[i].cantidad);
			}

			var chartdata = {
				labels: codigo,
				datasets : [
					{
						label: 'Codigo Cantidad',
						backgroundColor: 'rgba(200, 200, 200, 0.75)',
						borderColor: 'rgba(200, 200, 200, 0.75)',
						hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
						hoverBorderColor: 'rgba(200, 200, 200, 1)',
						data: cantidad
					}
				]
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'bar',
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});