package com.lp.ui.util;

import com.lp.ui.modelo.Correo;

import java.util.ArrayList;

/**
 *
 * @author MatheusJLV
 */
public class Resultados {
    public ArrayList<Correo> nada;
    public ArrayList<Correo> sospechoso;
    public ArrayList<Correo> falso;

    public Resultados() {
        this.nada = new ArrayList<Correo>();
        this.sospechoso = new ArrayList<Correo>();
        this.falso = new ArrayList<Correo>();
    }
    public void addCorreo(Correo mail, String clase)
    {
        if(clase=="nada"){
            addCorreoNada(mail, clase);
        }else if(clase=="sospechoso"){
            addCorreoSospechoso(mail, clase);
        }else if(clase=="falso"){
            addCorreofalso(mail, clase);
        } else{
            System.out.print("error");
        }
    }

    private void addCorreoNada(Correo mail, String clase) {
        this.nada.add(mail);
    }
    private void addCorreoSospechoso(Correo mail, String clase) {
        this.sospechoso.add(mail);
    }
    private void addCorreofalso(Correo mail, String clase) {
        this.falso.add(mail);
    }
    
    public String texto(){
        String texto;
        texto = "Nada separador1 ";
        for(Correo mail : nada){
            texto+=mail.getDesde()+" separador2 ";
            texto+=mail.getSubject()+" separador2 ";
            texto+=mail.getDate()+" separador2 ";
            texto+=mail.getContent()+" separador2 ";
            texto+="separador1";
        }
        texto+="\n";
        
        texto += "sospechoso separador1 ";
        for(Correo mail : sospechoso){
            texto+=mail.getDesde()+" separador2 ";
            texto+=mail.getSubject()+" separador2 ";
            texto+=mail.getDate()+" separador2 ";
            texto+=mail.getContent()+" separador2 ";
            texto+=" separador1";
        }
        texto+="\n";
        
        texto += "falso separador1";
        for(Correo mail : falso){
            texto+=mail.getDesde()+" separador2 ";
            texto+=mail.getSubject()+" separador2 ";
            texto+=mail.getDate()+" separador2 ";
            texto+=mail.getContent()+" separador2 ";
            texto+=" separador1";
        }
        return texto;
    }

    @Override
    public String toString() {
        return "Resultados{" + "nada=" + nada + ", sospechoso=" + sospechoso + ", falso=" + falso + '}';
    }

    String texto2() {
        String texto;
        texto = "Nada"+nada.size();
        texto+="\n";
        texto = "Sospechoso"+sospechoso.size();
        texto+="\n";
        texto = "Falso"+falso.size();
        return texto;
        
    }
    
    
}
