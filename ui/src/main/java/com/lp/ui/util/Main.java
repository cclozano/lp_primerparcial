package com.lp.ui.util;

import com.lp.ui.modelo.Correo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner; 
import java.util.regex.Pattern;
/**
 *
 * @author MatheusJLV
 */
public class Main {
    static Resultados resultados = new Resultados();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
				//System.out.println(Pattern.matches(".*Le recordamos que Banco Bolivariano NO envía comunicados ni vínculos solicitando claves personales, actualización de datos, sincronización de tarjetas o claves a través de ningún canal, sea este correo electrónico, mensaje de texto vía celular o llamada telefónica.*", "Hola, Le recordamos que Banco Bolivariano NO envía comunicados ni vínculos solicitando claves personales, actualización de datos, sincronización de tarjetas o claves a través de ningún canal, sea este correo electrónico, mensaje de texto vía celular o llamada telefónica" ));
        pruebaEscritura();
        //System.out.print(pruebaIngreso());
        
        
            // TODO code application logic here
    }
    public static void pruebaEscritura(){
        int band =1; 
        while(band==1){
						System.out.print("PRUEBA INGRESO");
            Correo mail = pruebaIngreso();
						System.out.print("MAIL");
            System.out.print(mail.toString());
						System.out.print("VALIDAR MAIL");
            resultados.addCorreo(mail, mail.validar(mail));
						System.out.print("RESULTADOS");
            System.out.print(resultados.toString());
            Scanner scan = new Scanner(System.in);
            System.out.println("Seguir ingresando mails?[y=1/n=0]");
            band = scan.nextInt();
        }
        escritura(resultados.texto2());
    }
    
    
    
    public static void escritura(String texto){
        BufferedWriter bw = null;
      try {
	 String mycontent = texto;
         //Specify the file name and path here
	 File file = new File("src/analisismail/analisisJava.txt");

	 /* This logic will make sure that the file 
	  * gets created if it is not present at the
	  * specified location*/
	  if (!file.exists()) {
	     file.createNewFile();
	  }

	  FileWriter fw = new FileWriter(file);
	  bw = new BufferedWriter(fw);
	  bw.write(mycontent);
          System.out.println("File written Successfully");

      } catch (IOException ioe) {
	   ioe.printStackTrace();
	}
	finally
	{ 
	   try{
	      if(bw!=null)
		 bw.close();
	   }catch(Exception ex){
	       System.out.println("Error in closing the BufferedWriter"+ex);
	    }
	}
    }
    
    public static Correo pruebaIngreso(){
        System.out.println("REGLAS DE ANALISIS: se pueden separar componentes del mail por separado, la resolucion"
                + "final sera la general, a partir de los resultados individuales. los resultados se dividen en real para "
                + "mails reales, sospechosos para mails no reales pero no necesariamente fraudulentos, y nada para"
                + "mails que no tienen que ver con el banco");
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese emisor de prueba");
        String de = scan.nextLine();
        System.out.println("Ingrese asunto de prueba");
        String asunto = scan.nextLine();
        System.out.println("Ingrese contenido de prueba");
        String contenido = scan.nextLine();
        Correo mail = new Correo();
        mail.setDesde(de);
        mail.setContent(contenido);
        mail.setSubject(asunto);
        System.out.println("validacion general");
        System.out.println(mail.validar(mail));
        System.out.println("validacion contenido");
        System.out.println(mail.validarContent(mail.getContent()));
        System.out.println("validacion emison");
        System.out.println(mail.validarFrom(mail.getDesde()));
        System.out.println("validacion asunto");
        System.out.println(mail.validarSubject(mail.getSubject()));
        return mail;
    }
    
    
}
