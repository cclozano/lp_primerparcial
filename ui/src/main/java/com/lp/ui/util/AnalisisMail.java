/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lp.ui.util;
import com.lp.ui.modelo.Correo;

import java.util.Scanner;

/**
 *
 * @author MatheusJLV
 */
public class AnalisisMail {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("REGLAS DE ANALISIS: se pueden separar componentes del mail por separado, la resolucion"
                + "final sera la general, a partir de los resultados individuales. los resultados se dividen en real para "
                + "mails reales, sospechosos para mails no reales pero no necesariamente fraudulentos, y nada para"
                + "mails que no tienen que ver con el banco");
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese emisor de prueba");
        String de = scan.next();
        System.out.println("Ingrese asunto de prueba");
        String asunto = scan.next();
        System.out.println("Ingrese contenido de prueba");
        String contenido = scan.next();
        Correo mail = new Correo();
        mail.setDesde(de);
        mail.setContent(asunto);
        mail.setSubject(contenido);
        System.out.println("validacion general");
        System.out.println(mail.validar(mail));
        System.out.println("validacion contenido");
        System.out.println(mail.validarContent(mail.getContent()));
        System.out.println("validacion emison");
        System.out.println(mail.validarFrom(mail.getDesde()));
        System.out.println("validacion asunto");
        System.out.println(mail.validarSubject(mail.getSubject()));
        
        

    }
    
}
