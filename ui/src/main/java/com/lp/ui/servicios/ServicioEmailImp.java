package com.lp.ui.servicios;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lp.ui.config.Parametros;
import com.lp.ui.modelo.Correo;
import com.lp.ui.modelo.RequestEmail;
import com.lp.ui.modelo.RequestSintaxis;
import com.lp.ui.modelo.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Component
public class ServicioEmailImp implements ServicioEmail{

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    Parametros parametros;

    @Override
    public List<Correo> consultar(String usuario, String password, String busqueda, int cantidad) {
        RequestEmail rq = new RequestEmail(usuario,password,busqueda,"" + cantidad);
        String uri = parametros.getUrl() + "email";
        //List<Correo> result = restTemplate.postForObject(uri , rq, List.class);
        String result = restTemplate.postForObject(uri , rq, String.class);


        ObjectMapper objectMapper = new ObjectMapper();
        List<Test> lista =Arrays.asList(new Test(), new Test(), new Test());
        String json= "";
        try {
            json = objectMapper.writeValueAsString(lista);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        Character ch = Character.MIN_VALUE;
        String xson = "";
        if(result!=null) xson = result.replace("\\","");

        try {
            xson = xson.substring(1,xson.length()-1);
            System.out.println(json);
            System.out.println(xson);

            List<Test> pojos2 = objectMapper.readValue(json, List.class);
            List<HashMap> pojos = objectMapper.readValue(xson, List.class);

            List<Correo> correos = new ArrayList<>();
            for(HashMap h : pojos)
            {
                Correo c = objectMapper.convertValue(h,Correo.class);
                correos.add(c);
            }
            return correos;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Correo> validar(List<Correo> lista) {


        return null;
    }
}
