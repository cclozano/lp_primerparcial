package com.lp.ui.servicios;

import com.lp.ui.modelo.Correo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ServicioEmail {

    List<Correo> consultar(String usuario, String password, String busqueda, int cantidad);
    List<Correo> validar(List<Correo> lista);
}
