package com.lp.ui.servicios;


import com.lp.ui.modelo.Token;

import java.util.List;

public interface ServicioInterprete {
    List<Token> revisarLexico(String script);
    int verificarPlagio(String codigo1, String codigo2);
    String analisisSintactico(String codigo);
}
