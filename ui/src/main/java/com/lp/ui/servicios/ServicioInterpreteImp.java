package com.lp.ui.servicios;


import com.lp.ui.config.Parametros;
import com.lp.ui.modelo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class ServicioInterpreteImp implements ServicioInterprete {

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    Parametros parametros;

    public List<Token> revisarLexico(String script) {

        RequestToken rq = new RequestToken();
        rq.setMessage(script);
        ResponseToken result = restTemplate.postForObject(parametros.getUrl() + "lex", rq, ResponseToken.class);
        if(result== null) return null;
        List<Token> tokens = new ArrayList<>();
        for (String[] token :
                result.getTokens()) {
            Token t = new Token();
            t.setNombre(token[0]);
            t.setValor(token[1]);
            tokens.add(t);
        }
        return tokens;
    }

    @Override
    public int verificarPlagio(String codigo1, String codigo2) {
        RequestPlagio rq = new RequestPlagio(codigo1,codigo2);
        int result = restTemplate.postForObject(parametros.getUrl() + "plagio", rq, int.class);
        return result;
    }

    @Override
    public String analisisSintactico(String codigo) {
        RequestSintaxis rq = new RequestSintaxis(codigo);
        String uri = parametros.getUrl() + "sintaxis";
        String result = restTemplate.postForObject(uri , rq, String.class);
        return result;
    }
}
