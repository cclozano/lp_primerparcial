package com.lp.ui;

import org.springframework.stereotype.Component;
import org.vaadin.spring.sidebar.annotation.SideBarSection;
import org.vaadin.spring.sidebar.annotation.SideBarSections;

@SideBarSections({
        @SideBarSection(id = Sections.OPERACIONES, caption = "Operaciones")
})
@Component
public class Sections {
    public static final String OPERACIONES = "operaciones";
}