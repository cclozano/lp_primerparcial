package com.lp.ui.views;

import com.lp.ui.Sections;
import com.lp.ui.config.Parametros;
import com.lp.ui.modelo.Token;
import com.lp.ui.servicios.ServicioInterprete;
import com.lp.ui.vaadintools.ToolBar;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.spring.sidebar.annotation.FontAwesomeIcon;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@SideBarItem(sectionId = Sections.OPERACIONES,caption = "PLY",order = 1)
@FontAwesomeIcon(FontAwesome.ANGELLIST)
@SpringView(name = PlyView.VIEW_NAME)
public class PlyView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "plyView";


    private ToolBar toolBar;
    TextArea area1 = new TextArea();
    TextArea area2 = new TextArea();

    Button lexicoButton = new Button("Analisis Lexico");
    Button sintaxisButton = new Button("Analisis Sintactico");
    Button plagioButton = new Button("Verificar Plagio");

    Button testButton = new Button(VaadinIcons.TEETH);

    Upload upload = new Upload();
    Upload upload2 = new Upload();

    @Autowired
    Parametros parametros;


    @Autowired
    ServicioInterprete servicioInterprete;


    public PlyView()
    {

        plagioButton.addClickListener(clickEvent -> {
           int result = servicioInterprete.verificarPlagio(area1.getValue(),area2.getValue());
            Notification.show("Plagio: " + result + "%", Notification.Type.ERROR_MESSAGE);
        });

        lexicoButton.setIcon(FontAwesome.PAPER_PLANE_O);
        sintaxisButton.setIcon(FontAwesome.CHECK);
        plagioButton.setIcon(FontAwesome.COMMENTING);
        lexicoButton.addClickListener(clickEvent -> {


            List<Token> tokens = servicioInterprete.revisarLexico(area1.getValue());
            if(tokens!=null) {
                final String[] str = {""};
                tokens.forEach(token ->
                {
                    str[0] = str[0] + "\n" +token.getNombre();
                });
                Notification.show(str[0], Notification.Type.ERROR_MESSAGE);
            }

        });

        sintaxisButton.addClickListener(clickEvent -> {
            String codigo = area1.getValue();
            String result = servicioInterprete.analisisSintactico(codigo);
            Notification.show("Revision: " + result, Notification.Type.ERROR_MESSAGE);
        });

        HorizontalLayout mainLayout = new HorizontalLayout();
        VerticalLayout area1Layout = new VerticalLayout();

        upload.setButtonCaption("Cargar Archivo");
        upload.setIcon(VaadinIcons.FILE);
        upload.setReceiver((fileName, mineType) -> {
            File file = new File(parametros.getReportPath() + fileName);
            FileOutputStream fop = null;
            try {

                fop = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return fop;
        });

        upload.addFinishedListener(finishedEvent -> {
            try {
                File file = new File(parametros.getReportPath() + finishedEvent.getFilename());
                final String fileAsString = FileUtils.readFileToString(file);
                area1.setValue(fileAsString);
                file.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        area1.setSizeFull();
        area1.setHeight("150%");

        area1.setValue("<?php $variable;?>");
        area2.setValue("<?php $variable; *** ?>");

        area1Layout.addComponents(upload, area1);
        VerticalLayout area2Layout = new VerticalLayout();
        upload2.setButtonCaption("Cargar Archivo");
        upload2.setIcon(VaadinIcons.FILE);
        upload2.setReceiver((fileName, mineType) -> {
            File file = new File(parametros.getReportPath() + fileName);
            FileOutputStream fop = null;
            try {

                fop = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return fop;
        });

        upload2.addFinishedListener(finishedEvent -> {

            try {
                File file = new File(parametros.getReportPath() + finishedEvent.getFilename());
                final String fileAsString = FileUtils.readFileToString(file);
                area2.setValue(fileAsString);
                file.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        area2.setSizeFull();
        area2Layout.addComponents(upload2,area2);
        mainLayout.addComponentsAndExpand(area1Layout,area2Layout);
        getToolBar().addDerecha(lexicoButton);
        getToolBar().addDerecha(sintaxisButton);
        getToolBar().addDerecha(plagioButton);
        testButton.addClickListener(clickEvent -> {
            try{
                String str = "runp "+ parametros.getLexerFile() +  " obtenerTokens:'"+ area1.getValue()+"'";
                System.out.println(str);
                Process p = Runtime.getRuntime().exec(str);
                final String[] tokens = {""};
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                in.lines().forEach(s -> {
                    System.out.println("value is : "+s);
                    tokens[0] = tokens[0] + "\n" +s;
                });
                Notification.show(tokens[0], Notification.Type.ERROR_MESSAGE);

            }catch(Exception e){}
        });


        //getToolBar().addDerecha(testButton);
        this.add(getToolBar(),mainLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    public ToolBar getToolBar()
    {
        if(toolBar ==null)
            toolBar = new ToolBar();
        return toolBar;
    }
}
