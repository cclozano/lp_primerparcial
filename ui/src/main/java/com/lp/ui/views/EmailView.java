package com.lp.ui.views;

import com.lp.ui.Sections;
import com.lp.ui.config.Parametros;
import com.lp.ui.modelo.Correo;
import com.lp.ui.servicios.ServicioEmail;
import com.lp.ui.vaadintools.ToolBar;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.sidebar.annotation.FontAwesomeIcon;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.List;

@SideBarItem(sectionId = Sections.OPERACIONES,caption = "EMAIL",order = 1)
@FontAwesomeIcon(FontAwesome.MAIL_REPLY_ALL)
@SpringView(name = EmailView.VIEW_NAME)
public class EmailView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "emailView";


    private ToolBar toolBar;
    TextArea area1 = new TextArea();
    TextArea area2 = new TextArea();

    Button consultarButton = new Button("Consultar Correos");
    Button analizarButton = new Button("Analisis");

    TextField usuario = new TextField("Usuario");
    PasswordField password = new PasswordField("Password");
    TextField busqueda = new TextField("busqueda");
    TextField cantidad = new TextField("cantidad");


    @Autowired
    Parametros parametros;


    @Autowired
    ServicioEmail servicioEmail;


    Grid<Correo> grid = new Grid<>();
    public EmailView()
    {
        consultarButton.addClickListener(clickEvent -> {
            int n = Integer.parseInt(cantidad.getValue());

            List<Correo> result = servicioEmail.consultar(usuario.getValue(),password.getValue(),busqueda.getValue(),n);

            for(Correo m : result)
            {
                System.out.println(m.validar(m));
            }
            grid.setItems(result);

        });



        consultarButton.setIcon(FontAwesome.PAPER_PLANE_O);
        analizarButton.setIcon(FontAwesome.CHECK);

        HorizontalLayout main = new HorizontalLayout();
        VerticalLayout mainLayout = new VerticalLayout();
        VerticalLayout gr = new VerticalLayout();

        main.addComponents(mainLayout,gr);
        gr.addComponentsAndExpand(grid);
        grid.addColumn(correo -> correo.getDesde()).setCaption("From");
        grid.addColumn(correo -> correo.getTo()).setCaption("To");
        grid.addColumn(correo -> correo.getDate()).setCaption("Fecha");
        grid.addColumn(correo -> correo.getEstado()).setCaption("Estado");
        mainLayout.addComponents(usuario,password,busqueda,cantidad);

        getToolBar().addDerecha(consultarButton);
        getToolBar().addDerecha(analizarButton);
        this.add(getToolBar(),main);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    public ToolBar getToolBar()
    {
        if(toolBar ==null)
            toolBar = new ToolBar();
        return toolBar;
    }
}
