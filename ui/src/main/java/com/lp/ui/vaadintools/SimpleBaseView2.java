package com.lp.ui.vaadintools;

import com.vaadin.ui.Grid;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.text.SimpleDateFormat;

public abstract class SimpleBaseView2<E> extends MVerticalLayout {

    private ToolBar toolBar;
    protected Grid<E> grid;

    protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    protected SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public SimpleBaseView2()
    {
        this.grid = getGrid();
        this.grid.setWidth("100%");
        this.grid.setHeight("100%");
        add(getToolBar(),grid)
                .expand(grid)
                .withWidth("100%")
                .withHeight("100%");


    }

    public ToolBar getToolBar()
    {
        if(toolBar ==null)
            toolBar = new ToolBar();
        return toolBar;
    }



    public abstract Grid<E> getGrid();
}
