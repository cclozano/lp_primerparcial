package com.lp.ui.vaadintools;

public class EntityModifiedEvent<E> {
    private final E entity;
    public EntityModifiedEvent(E e) {
        this.entity = e;
    }
    public E getEntity() {
        return entity;
    }
}