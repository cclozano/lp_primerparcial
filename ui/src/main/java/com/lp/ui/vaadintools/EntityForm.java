package com.lp.ui.vaadintools;


import com.vaadin.ui.Window;

public interface EntityForm<E> {

    void setEntity(E entity);
    Window openInModalPopup();
    void closePopup();
    Window getPopup();
}
