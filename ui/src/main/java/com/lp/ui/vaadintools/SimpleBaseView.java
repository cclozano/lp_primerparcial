package com.lp.ui.vaadintools;


import com.vaadin.ui.components.grid.GridSelectionModel;
import com.vaadin.ui.components.grid.SingleSelectionModel;
import org.vaadin.viritin.grid.MGrid;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.text.SimpleDateFormat;

public abstract class SimpleBaseView<E> extends MVerticalLayout {
    private ToolBar toolBar;
    protected MGrid<E> grid;

    protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    protected SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public SimpleBaseView()
    {
        this.grid = getGrid();
        this.grid.setWidth("100%");
        this.grid.setHeight("100%");
        add(getToolBar(),grid)
                .expand(grid)
                .withWidth("100%")
                .withHeight("100%");


    }

    public ToolBar getToolBar()
    {
        if(toolBar ==null)
            toolBar = new ToolBar();
        return toolBar;
    }



    public abstract MGrid<E> getGrid();

    public E getSelectedItem()
    {
        GridSelectionModel<E> model = this.grid.getSelectionModel();
        if (!(model instanceof SingleSelectionModel)) {
            throw new IllegalStateException("Grid is not in single select mode, it needs to be explicitly set to such with setSelectionModel(SingleSelectionModel) before being able to use single selection features.");
        } else {
            return this.grid.getSelectedItems().stream().findFirst().get();
        }
    }


}