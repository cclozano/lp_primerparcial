package com.lp.ui.api;

import com.lp.ui.modelo.Correo;
import com.lp.ui.modelo.RequestEmail;
import com.lp.ui.servicios.ServicioEmail;
import com.vaadin.server.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("lenguajes/correo")
@Api(value="lenguajes/correo/", description="Operaciones con correo")
public class CorreoController {

    public static final Logger logger = LoggerFactory.getLogger(CorreoController.class);

    @Autowired
    ServicioEmail servicioEmail;

    @ApiOperation(value = "Consultar y analizar correos",response = Correo.class)
    @RequestMapping(value = "/consultar", method = RequestMethod.POST)
    public ResponseEntity<?> consultar(@RequestBody RequestEmail request) {
        logger.info("Consultando {}", request);
        System.out.println(request);

            List<Correo> correos = Arrays.asList(new Correo(), new Correo());
            return new ResponseEntity(correos, HttpStatus.OK);
            //if(false)throw new ServiceException("El Correo ya esta registrado");
            /*Correo p = articuloRepository.findByCodigo(articulo.getCodigo());
            if (p!=null)
                throw new ServiceException("El Articulo ya esta registrado");
            else
                articuloRepository.save(articulo);*/

    }

    @ApiOperation(value = "Consultar y analizar correos",response = Correo.class)
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ResponseEntity<?> test(@RequestBody RequestEmail request) {
        logger.info("Consultando {}", request);
        System.out.println(request);

        return new ResponseEntity(new Correo(), HttpStatus.OK);
        //if(false)throw new ServiceException("El Correo ya esta registrado");
            /*Correo p = articuloRepository.findByCodigo(articulo.getCodigo());
            if (p!=null)
                throw new ServiceException("El Articulo ya esta registrado");
            else
                articuloRepository.save(articulo);*/

    }


    @ApiOperation(value = "Consultar y analizar correos",response = Correo.class)
    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    public ResponseEntity<?> test1() {

        return new ResponseEntity(new Correo(), HttpStatus.OK);

    }



    @ApiOperation(value = "Consultar y clasificar correos")
    @RequestMapping(value = "/verificar", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity verificar(@RequestBody RequestEmail request){
        try {
            List<Correo> correos = servicioEmail.consultar(request.getUser(),request.getPassword(),request.getBusqueda(),Integer.parseInt(request.getCantidad()));

            for(Correo m : correos)
            {
                System.out.println(m.validar(m));
            }


            return new ResponseEntity(correos, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new CustomErrorType("Error: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
