package com.lp.ui.modelo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Test {
    private  String date = "date";
    private  String to = "to";
    private  String content = "content";
    private  String desde = "desde";
    private  String subject = "subject";
}
