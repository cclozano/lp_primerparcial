package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestEmail {

    private String user;
    private String password;
    private String busqueda;
    private String cantidad;

    public RequestEmail(String user, String password, String busqueda, String cantidad) {
        this.user = user;
        this.password = password;
        this.busqueda = busqueda;
        this.cantidad = cantidad;
    }
}
