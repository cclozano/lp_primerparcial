/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

import java.util.regex.Pattern;

/**
 *
 * @author MatheusJLV
 */
@Getter @Setter
public class Correo {
    /*String from_;
    String to_;
    String subject_;
    String content;
    String date_;*/

    private  String date = "date";
    private  String to = "to";
    private  String content = "content";
    private  String desde = "Correo de origen";
    private  String subject = "subject";

    private Estado estado;


    public enum Estado
    {
        Correcto,
        Sospechoso,
        Maligno
    }
    /*
        Bolivariano         info@bolivariano.com
        Le recordamos que Banco Bolivariano NO envía comunicados ni vínculos solicitando claves personales, actualización de datos, sincronización de tarjetas o claves a través de ningún canal, sea este correo electrónico, mensaje de texto vía celular o llamada telefónica.
        Internacional       baninternews@bancointernacional.com.ec
        No solicitamos claves. No pedimos actualización de datos. No enviamos links por correo electrónico.
        */
    public Correo(String from, String to, String subject, String content, String date) {
        this.desde = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.date = date;
    }

    public Correo() {
    }


    public String validar(Correo mail){
        String vfrom;
        String vsubject;
        String vcontent;
        vfrom = validarFrom(mail.desde);
        vsubject = validarSubject(mail.subject);
        vcontent = validarContent(mail.content);
        String validacion;
        if(vfrom == "falso" ||vsubject == "falso" ||vcontent == "falso" ){
            this.estado = Estado.Maligno;
            return "falso";
        } else if(vfrom == "nada" &&vsubject == "nada" &&vcontent == "nada" ){
            this.estado = Estado.Correcto;
            return "nada";
        }else if(vfrom == "real" && vsubject == "real" && vcontent == "real" ){
            this.estado = Estado.Sospechoso;
            return "real";
        }
        return "sospechoso";
    }
    public String validarFrom(String fr){
        if(fr.contains("bolibariano") || fr.contains("volibariano")|| fr.contains("volivariano")){
            return "falso";
        }
        if (fr.contains("@bolivariano.com") || fr.contains("@bancointernacional.com.ec")){
            return "real";
        } else if(fr.contains("bolivariano") || fr.contains("bancointernacional") || fr.contains("banco")){
            return "sospechoso";
        }
        return "nada";
    }
    public String validarSubject(String su){
        if(su.contains("bolibariano") || su.contains("volibariano")|| su.contains("volivariano")){
            return "falso";
        }
        return "nada";
    }
    public String validarContent(String co){
        if(co.contains("bolibariano") || co.contains("volibariano")|| co.contains("volivariano")){
            return "falso";
        }
        System.out.println("Vcontent2");
        //boolean b = Pattern.matches("b*a", "aaaaab");
        //Pattern.matches(".*.*", co) www.bancointernacional.com.ec
        if(co.contains("Le recordamos que Banco Bolivariano NO envía comunicados ni vínculos solicitando claves personales, actualización de datos, sincronización de tarjetas o claves a través de ningún canal, sea este correo electrónico, mensaje de texto vía celular o llamada telefónica.") || co.contains("No solicitamos claves. No pedimos actualización de datos. No enviamos links por correo electrónico.")){
            System.out.println("pie de pag detectado");
            if(co.contains("www") || co.contains(".com")){
                System.out.println("link detectado");
                if(!co.contains("bancointernacional.com.ec") && !co.contains("bolivariano.com")){
                    return "falso";
                }
                return "real";

            }
            return "real";
        } else if(co.contains("bolivariano") ||co.contains("banco internacional")){
            return "sospechoso";
        }
        return "nada";
    }

    @Override
    public String toString() {
        return "Mail{" + "from_=" + desde + ", to_=" + to + ", subject_=" + subject + ", content=" + content + ", date_=" + date+ '}';
    }

}
