package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestToken {
    private String message;
}
