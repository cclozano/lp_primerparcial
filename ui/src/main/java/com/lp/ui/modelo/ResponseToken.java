package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponseToken {

    private String[][] tokens;
}
