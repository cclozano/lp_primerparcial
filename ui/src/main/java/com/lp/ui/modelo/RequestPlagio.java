package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestPlagio {
    private String codigo1;
    private String codigo2;

    public RequestPlagio(String codigo1, String codigo2) {
        this.codigo1 = codigo1;
        this.codigo2 = codigo2;
    }
}
