package com.lp.ui.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestSintaxis {
    private String codigo;

    public RequestSintaxis(String codigo) {
        this.codigo = codigo;
    }
}
