package com.lp.ui.modelo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Token {
    private String nombre;
    private String valor;
}
